class SessionsControllerTest < ActionController::TestCase

  # A helper to format a Google response object from a user's attributes
  # Note: could not seem to extend the User class as I had hoped
  def to_request user
    request = {
      :provider => "google_oauth2",
      :uid => user.uid,
      :info => {
        :name => user.name,
        :email => user.email,
        :first_name => user.first_name,
        :last_name => user.last_name,
        :image => user.image
      },
      :credentials => {
        :token => user.token,
        :refresh_token => user.refresh_token,
        :expires_at => Time.at(1419290669).to_datetime,
        :expires => true
      },
      :extra => {
        :raw_info => {
          :sub => "123456789",
          :email => user.email,
          :email_verified => true,
          :name => user.name,
          :given_name => user.first_name,
          :family_name => user.last_name,
          :profile => "https://plus.google.com/123456789",
          :picture => user.image,
          :gender => "male",
          :birthday => "0000-06-25",
          :locale => "en",
          :hd => "company_name.com"
        }
      }
    }.with_indifferent_access
  end

  # A simple test to retrieve the index action
  test "should get index" do
    get "profiles#index"
    assert_response :success
  end

  # test "should route root_path to sessions#index" do
  #   assert_routing({ path: '/', method: :get }, { controller: 'profile', action: 'index' })
  # end  

  # test "should raise an exception if request.env['omniauth.auth'] is not defined" do
  #   assert_raises(RuntimeError) { get :create, provider: "google_oauth2" }
  # end

  # test "should create a new user on first login" do
  #   @request.env['omniauth.auth'] = {
  #       :provider => "google_oauth2",
  #       :uid => "123456789",
  #       :info => {
  #           :name => "John Doe",
  #           :email => "john@company_name.com",
  #           :first_name => "John",
  #           :last_name => "Doe",
  #           :image => "https://lh3.googleusercontent.com/url/photo.jpg"
  #       },
  #       :credentials => {
  #           :token => "token",
  #           :refresh_token => "another_token",
  #           :expires_at => 1354920555,
  #           :expires => true
  #       },
  #       :extra => {
  #           :raw_info => {
  #               :sub => "123456789",
  #               :email => "user@domain.example.com",
  #               :email_verified => true,
  #               :name => "John Doe",
  #               :given_name => "John",
  #               :family_name => "Doe",
  #               :profile => "https://plus.google.com/123456789",
  #               :picture => "https://lh3.googleusercontent.com/url/photo.jpg",
  #               :gender => "male",
  #               :birthday => "0000-06-25",
  #               :locale => "en",
  #               :hd => "company_name.com"
  #           }
  #       }
  #   }.with_indifferent_access
  #   user_count = User.count

  #   get :create, provider: "google_oauth2"

  #   assert_equal user_count+1, User.count
  #   assert_equal User.find_by_uid("123456789"), assigns["current_user"]
  # end

  # test "should set session['current_user_id']" do
  #   # cookies[:sharey_session_cookie] = users(:pam).sharey_session_cookie
  #   pam = users(:pam)
  #   @request.env['omniauth.auth'] = to_request pam
  #   user_count = User.count

  #   get :create, provider: "google_oauth2"
  #   assert_equal users(:pam), assigns["current_user"]

  #   assert_equal user_count, User.count
  # end

  # test "should have access to helper current_user generated from session[current_user_id]" do
  #   # cookies[:sharey_session_cookie] = users(:pam).sharey_session_cookie
  #   session['current_user_id'] = user(:pam).id

  #   get :index
  #   assert_equal users(:pam), assigns["current_user"]
  # end

  # test "should destroy session and redirect to root_path" do
  #   # cookies[:sharey_session_cookie] = users(:pam).sharey_session_cookie
  #   session['current_user_id'] = users(:pam).id
  #   pam = users(:pam)
  #   @request.env['omniauth.auth'] = to_request pam

  #   get :create, provider: "google_oauth2"

  #   get :destroy
  #   assert_redirected_to root_path
  #   assert_nil session['current_user_id']
  # end
end