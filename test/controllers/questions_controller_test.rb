require 'test_helper'

class QuestionsControllerTest < ActionDispatch::IntegrationTest
	# before do 
 #  		request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:google] 
	# end

	def to_request user
		request = {
		:provider => "google_oauth2",
		:uid => user.uid,
		:info => {
			:name => user.name,
			:email => user.email,
			:first_name => user.first_name,
			:last_name => user.last_name,
			:image => user.image,
		},
		:credentials => {
			:token => user.token,
			:refresh_token => user.refresh_token,
			:expires_at => Time.at(1419290669).to_datetime,
			:expires => true
		},
		:extra => {
			:raw_info => {
	  			:sub => "123456789",
	  			:email => user.email,
	  			:email_verified => true,
	  			:name => user.name,
	  			:given_name => user.first_name,
	  			:family_name => user.last_name,
	  			:profile => "https://plus.google.com/123456789",
	  			:picture => user.image,
			  	:gender => "male",
			  	:birthday => "0000-06-25",
			  	:locale => "en",
			  	:hd => "company_name.com"
				}
			}
		}.with_indifferent_access
	end	

	test "should get index" do
		# let(:user) { FactoryGirl.create(:user, full_name: "Jaye Hernandez") }
		get questions_path
		assert_response :success
	end

	# test "can create a question" do 
	# 	get "/questions/new"
	# 	assert_response :success

	# 	post "/questions",
	# 		params: {question: {questionDesription: "Can add question", area: "Areaaa", skill: "Skillll" }}
	# 	assert_response :redirect
	# 	follow_redirect!
	# 	assert_response :success
	# 	assert_select "p", "Can add question"	
	# end
	test "visit questions" do
	  	assert_routing '/questions', {controller: 'questions', action: 'index'}
	end

 	test "user cant go to questions" do
		assert_redirected_to(controller: 'profiles', action: 'index')
	end
end