require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "should fail if updated name is empty" do
    name = "user"
    user = User.create(:full_name => name)
    user.update(:full_name => "")
    assert_not user.valid?
  end

  test "should fail if name length is greater than 70" do
    name = "user"
    user = User.create(:full_name => name)
    new_name = rand(36**71).to_s(36)
    user.update(:full_name => new_name)
    assert_not user.valid?
  end

  test "should fail if name has the wrong format" do
    name = "user"
    user = User.create(:full_name => name)
    user.update(:full_name => "Fail_1")
    assert_not user.valid?
  end

  test "should update if name has the right format" do
    name= "user"
    user = User.create(:full_name => name)
    user.update(:full_name => "new")
    assert_not_equal name, user.full_name
  end

  test "should stay the same if name is not changed" do
    name= "user"
    user = User.create(:full_name => name)
    user.update(:full_name => "user")
    assert_equal name, user.full_name
  end

  test "should fail if employee number has special characters" do
    user = User.create(:full_name => "name")
    user.update(:employee_number => "123-456")
    assert_not user.valid?
  end

  test "should fail if employee number has letters" do
    user = User.create(:full_name => "name")
    user.update(:employee_number => "123a")
    assert_not user.valid?
  end

  test "should fail if employee number is just 0[s]" do
    user = User.create(:full_name => "name")
    user.update(:employee_number => "00000")
    assert_not user.valid?
  end

  test "should fail if employee number has more than 11 characters" do
    user = User.create(:full_name => "name")
    user.update(:employee_number => "000123456789")
    assert_not user.valid?
  end

  test "should allow employee number to be blank" do
    user = User.create(:full_name => "name")
    user.update(:employee_number => " ")
    assert user.valid?
  end

  test "should update if employee number has the right format" do
    num = "12345"
    user = User.create(:full_name => "name", :employee_number => num)
    user.update(:employee_number => "6789")
    assert_not_equal num, user.employee_number
  end

  test "should stay the same if employee number is not changed" do
    num = "12345"
    user = User.create(:full_name => "name", :employee_number => num)
    user.update(:employee_number => "12345")
    assert_equal num, user.employee_number
  end

  test "should fail if unit length is greater than 70" do
    user = User.create(:full_name => "name")
    new_unit = rand(36**71).to_s(36)
    user.update(:unit => new_unit)
    assert_not user.valid?
  end

  test "should fail if unit has the wrong format" do
    user = User.create(:full_name => "name")
    user.update(:unit => "Fail_1")
    assert_not user.valid?
  end

  test "should allow unit to be blank" do
    user = User.create(:full_name => "name", :unit => " ")
    assert user.valid?
  end

  test "should update if unit has the right format" do
    unit = "unit"
    user = User.create(:full_name => "name", :unit => unit)
    user.update(:unit => "new")
    assert_not_equal unit, user.unit
  end

  test "should stay the same if unit is not changed" do
    unit = "unit"
    user = User.create(:full_name => "name", :unit => unit)
    user.update(:unit => "unit")
    assert_equal unit, user.unit
  end

  test "should fail if designation has the wrong format" do
    user = User.create(:full_name => "name")
    user.update(:designation => "Fail_1")
    assert_not user.valid?
  end

  test "should allow designation to be blank" do
    user = User.create(:full_name => "name", :designation => " ")
    assert user.valid?
  end

  test "should update if designation has the right format" do
    designation = "designation"
    user = User.create(:full_name => "name", :designation => designation)
    user.update(:designation => "new")
    assert_not_equal designation, user.designation
  end

  test "should stay the same if designation is not changed" do
    designation = "designation"
    user = User.create(:full_name => "name", :designation => designation)
    user.update(:designation => "designation")
    assert_equal designation, user.designation
  end

  test "should fail if marital status is not in options" do
    name= "user"
    user = User.create(:full_name => name, :marital_status => "Fail")
    assert_not user.valid?
  end

  test "should allow marital status to be blank" do
    name= "user"
    user = User.create(:full_name => name, :marital_status => " ")
    assert user.valid?
  end

  test "should pass if marital status is in options" do
    name= "user"
    user = User.create(:full_name => name, :marital_status => "Single")
    assert user.valid?
  end

  test "should fail if date does not exist" do
    name= "user"
    user = User.create(:full_name => name, :birth_date => "1994-02-30")
    assert_not user.valid?
  end

  test "should fail if date before Date.today - 100.year" do
    name= "user"
    user = User.create(:full_name => name, :birth_date => "1900-01-01")
    assert_not user.valid?
  end

  test "should fail if date is after Date.today - 10.year" do
    name= "user"
    user = User.create(:full_name => name, :birth_date => "2017-01-01")
    assert_not user.valid?
  end

  test "should allow empty dates" do
    name= "user"
    user = User.create(:full_name => name, :birth_date => " ")
    assert user.valid?
  end

  test "should pass if date field is valid" do
    name= "user"
    user = User.create(:full_name => name, :birth_date => "1994-01-30")
    assert user.valid?
  end

  test "should allow sex to be blank" do
    name= "user"
    user = User.create(:full_name => name, :sex => " ")
    assert user.valid?
  end

  test "should pass if sex is in options" do
    name= "user"
    user = User.create(:full_name => name, :sex => "Male")
    assert user.valid?
  end

  test "should fail if sex is not in options" do
    name= "user"
    user = User.create(:full_name => name, :sex => "Fail")
    assert_not user.valid?
  end

  test "should pass if year is blank" do
    name= "user"
    user = User.create(:full_name => name, :year => "")
    assert user.valid?
  end

  test "should pass if year is equal to 0" do
    name= "user"
    user = User.create(:full_name => name, :year => "0")
    assert user.valid?
  end

  test "should pass if year is just above 0" do
    name= "user"
    user = User.create(:full_name => name, :year => "1")
    assert user.valid?
  end

  test "should pass if year is just below 100" do
    name= "user"
    user = User.create(:full_name => name, :year => "99")
    assert user.valid?
  end

  test "should pass if year is equal to 100" do
    name= "user"
    user = User.create(:full_name => name, :year => "100")
    assert user.valid?
  end

  test "should fail if year is less than 0" do
    name= "user"
    user = User.create(:full_name => name, :year => "-1")
    assert_not user.valid?
  end

  test "should fail if year is greater than 100" do
    name= "user"
    user = User.create(:full_name => name, :year => "101")
    assert_not user.valid?
  end

  test "should fail if year is not a number" do
    name= "user"
    user = User.create(:full_name => name, :year => "Fail")
    assert_not user.valid?
  end

  test "should fail if year is not an integer" do
    name= "user"
    user = User.create(:full_name => name, :year => "1.5")
    assert_not user.valid?
  end

  test "should pass if month is blank" do
    name= "user"
    user = User.create(:full_name => name, :month => "")
    assert user.valid?
  end

  test "should pass if month is equal to 0" do
    name= "user"
    user = User.create(:full_name => name, :month => "0")
    assert user.valid?
  end

  test "should pass if month is just above 0" do
    name= "user"
    user = User.create(:full_name => name, :month => "1")
    assert user.valid?
  end

  test "should pass if month is just below 11" do
    name= "user"
    user = User.create(:full_name => name, :month => "10")
    assert user.valid?
  end

  test "should pass if month is equal to 11" do
    name= "user"
    user = User.create(:full_name => name, :month => "11")
    assert user.valid?
  end

  test "should fail if month is less than 0" do
    name= "user"
    user = User.create(:full_name => name, :month => "-1")
    assert_not user.valid?
  end

  test "should fail if month is greater than 11" do
    name= "user"
    user = User.create(:full_name => name, :month => "12")
    assert_not user.valid?
  end

  test "should fail if month is not a number" do
    name= "user"
    user = User.create(:full_name => name, :month => "Fail")
    assert_not user.valid?
  end

  test "should fail if month is not an integer" do
    name= "user"
    user = User.create(:full_name => name, :month => "1.5")
    assert_not user.valid?
  end
end
