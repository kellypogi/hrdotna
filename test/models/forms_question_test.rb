require 'test_helper'

class FormsQuestionTest < ActiveSupport::TestCase
  test "should exist when form-question relationship is created" do
    question = Question.create(:questionDescription => "q1", :skill => "s1")
    form = Form.create(:title => "test",:question_ids => [question.id] )
    formquestion = FormsQuestion.select(:form_id => form.id, :question_id => question.id)
    assert formquestion.exists?
  end
end
