require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  test "should not save without skill and question description" do
    question = Question.new
    assert_not question.save
  end

  test "should save with question description and skill" do
    question = Question.create(:questionDescription => "q1",:skill => "s1")
    assert question.valid?
  end

  test "should not save with question description only" do
    question = Question.create(:questionDescription => "q1")
    assert_not question.valid?
  end

  test "should not save with skill only" do
    question = Question.create(:skill => "s1")
    assert_not question.valid?
  end

  test "should update if question description is edited" do
    dummy = "q1"
    question = Question.create(:questionDescription => dummy, :skill => "s1")
    question.update(:questionDescription => "new")
    assert_not_equal dummy, question.questionDescription
  end

  test "should update if skill is edited" do
    dummy = "s1"
    question = Question.create(:questionDescription => "q1", :skill => dummy)
    question.update(:skill => "new")
    assert_not_equal dummy, question.skill
  end

  test "should destroy question" do
    question = Question.create(:questionDescription => "q1", :skill => "s1")
    assert_difference('Question.count', -1) do
      question.destroy
    end
  end
end
