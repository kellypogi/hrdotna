require 'test_helper'

class TrainingTest < ActiveSupport::TestCase
    test "should fail if updated title is empty" do
      title = "training"
      tr = Training.create(:title => name)
      tr.update(:title => "")
      assert_not tr.valid?
    end

    test "should update title is changed" do
      title= "training"
      tr = Training.create(:title => name)
      tr.update(:title => "new")
      assert_not_equal title, tr.title
    end

    test "should stay the same if title is not changed" do
        title= "training"
        tr = Training.create(:title => name)
        tr.update(:title => "training")
        assert_equal title, tr.title
    end

    test "should fail if title length is greater than 100" do
      title = "training"
      tr = Training.create(:title => name)
      newTitle = rand(36**101).to_s(36)
      tr.update(:title => newTitle)
      assert_not tr.valid?
    end
    
    test "should pass if there is one question ID" do
      title = "training"
      q1 = Question.create(:questionDescription => "Question 1")
      tr = Training.create(:title => title, :question_id => q1.id)
      assert tr.valid?
    end
end
