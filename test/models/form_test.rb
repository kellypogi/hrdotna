require 'test_helper'

class FormTest < ActiveSupport::TestCase
  test "should not save without form title" do
    form = Form.new
    assert_not form.save
  end

  test "should save with title" do
    form = Form.create(:title => "test")
    assert form.valid?
  end

  test "should update if title is edited" do
    title = "test"
    form = Form.create(:title => title)
    form.update(:title => "new")
    assert_not_equal title, form.title
  end

  test "should update if description is edited" do
    description = "test"
    form = Form.create(:title => "title", :description => description)
    form.update(:description => "new")
    assert_not_equal description, form.description
  end

  test "should update if deadline is edited" do
    deadline = "test"
    form = Form.create(:title => "title", :deadline => deadline)
    form.update(:deadline => "new")
    assert_not_equal deadline, form.deadline
  end

  test "should not save with description only" do
    form = Form.create(:description => "test")
    assert_not form.valid?
  end

  test "should not save with deadline only" do
    form = Form.create(:deadline => "test")
    assert_not form.valid?
  end

  test "should not save with deadline and description only" do
    form = Form.create(:description => "test", :deadline => "test")
    assert_not form.valid?
  end

  test "should destroy form" do
    form = Form.create(:title => "test")
    assert_difference('Form.count', -1) do
      form.destroy
    end
  end
end
