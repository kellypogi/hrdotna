class FormsController < ApplicationController
  before_action :authorize_admin
  def index
    @forms = Form.all
  end
  def show
    @form = Form.find(params[:id])
    @questions = Question.all
  end
  def new
    # if (!current_user.is_admin)
    #   redirect_to forms_path
    # end
    @form = Form.new
  end
  def edit
    @form = Form.find(params[:id])
  end

  def create
    # if (!current_user.is_admin)
    #   redirect_to forms_path
    # end
    @form = Form.new(form_params)
    if @form.valid?
      @form.save
      flash[:success_header] = @form.title + " has been created!"
      flash[:success_body] = "The form has been saved to the database."
      redirect_to forms_path
    else
      render 'new'
    end
  end
  
  def update
    @form = Form.find(params[:id])
    if @form.update(form_params)
      flash[:success_header] = @form.title + " has been updated!"
      flash[:success_body] = "The changes have been saved to the database."
      redirect_to @form
    else
      render 'edit'
    end
  end

  def destroy
    # if (!current_user.is_admin)
    #   redirect_to forms_path
    # end
    @form = Form.find(params[:id])
    @form.destroy
    flash[:success_header] = @form.title + " has been deleted!"
    flash[:success_body] = "The changes have been saved to the database."
    redirect_to forms_path
  end
  private
    def form_params
      params.require(:form).permit(:title, :description, :deadline, :question_ids => [])
    end

    def authorize_admin
      if !current_user.is_admin?
        flash[:error_header] = "Sorry, you are not authorized to view this page."
        flash[:error_body] = "You have been redirected to your profile."
        redirect_to '/profile'
      end
    end
end
