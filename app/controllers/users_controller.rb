class UsersController < ApplicationController
    before_action   :authorize_admin, except:[:set_admin,:unset_admin]
    helper_method :sort_column, :sort_direction
    def index
        @users = User.order(sort_column + " " + sort_direction)
    end
    def show
        @user = User.find(params[:user_id])
        @date_before = Date.today-50.year..Date.today
        @trainingsAttended = TrainingLog.joins(:training).where(user_id: @user.id, recommendation: "Going", trainings: {date: @date_before})
    end

    def set_admin
        @user = User.find(params[:user_id])
        @user.update(is_admin: true)
        redirect_to '/users/' + @user.id.to_s
    end

    def unset_admin
      @user = User.find(params[:user_id])
      @user.update(is_admin: false)
      redirect_to '/users/' + @user.id.to_s
    end
    private
    def sort_column
        User.column_names.include?(params[:sort]) ? params[:sort] : "full_name"
    end
    def sort_direction
        %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

    private
        def authorize_admin
          if !current_user.is_admin?
            flash[:error_header] = "Sorry, you are not authorized to view this page."
            flash[:error_body] = "You have been redirected to your profile."
            redirect_to '/profile'
          end
        end
end
