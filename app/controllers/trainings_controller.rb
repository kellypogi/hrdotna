class TrainingsController < ApplicationController

  before_action :authorize_admin, only: [:show, :new, :edit, :update, :destroy]

  # GET /trainings
  # GET /trainings.json
  def index
    @trainings = Training.all
    @questions = Question.all
    @forms = Form.all
    @trainingLogs = TrainingLog.all
    @invited = @trainingLogs.where(recommendation: "Invited", user_id: current_user.id);
    @date_after = Date.today..Date.today+50.year
    @going = TrainingLog.joins(:training).where(user_id: current_user.id, recommendation: "Going", trainings: {date: @date_after})
  end

  # GET /trainings/1
  # GET /trainings/1.json
  def show
    @training = Training.find(params[:id])
    @questions = Question.all
    @forms = Form.all
    @trainingLogs = TrainingLog.all
    @invites = @trainingLogs.where(training_id: @training.id);
    @recom = @trainingLogs.where(training_id: @training.id, recommendation: "Recommended");  #filters like !recommended or going to be added later
  end

  # GET /trainings/new
  def new
    @training = Training.new
    @questions = Question.all
    @forms = Form.all
  end

  def invite
    @traininglog = TrainingLog.find(params[:id])
    @traininglog.recommendation = "Invited"
    @traininglog.save
    redirect_back(fallback_location: root_path)
  end

  def reject
    @traininglog = TrainingLog.find(params[:id])
    @traininglog.recommendation = "Rejected"
    @traininglog.save
    redirect_back(fallback_location: root_path)
  end

  def accept
    @traininglog = TrainingLog.find(params[:id])
    @traininglog.recommendation = "Going"
    @traininglog.save
    redirect_back(fallback_location: root_path)
  end

  def deny
    @traininglog = TrainingLog.find(params[:id])
    @traininglog.recommendation = "Not Going"
    @traininglog.save
    redirect_back(fallback_location: root_path)
  end

  # GET /trainings/1/edit
  def edit
    @training = Training.find(params[:id])
  end

  # POST /trainings
  # POST /trainings.json
  def create
    @training = Training.new(training_params)
    respond_to do |format|
      if @training.save
        @trainingref = @training.form
        @trainingref.training_id = @training.id
        @trainingref.save

        format.html { redirect_to trainings_path, notice: 'Training was successfully created.' }
        format.json { render :show, status: :created, location: @training }
      else
        format.html { render :new }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trainings/1
  # PATCH/PUT /trainings/1.json
  def update
    @training = Training.find(params[:id])
    respond_to do |format|
      if @training.update(training_params)
        @trainingref = @training.form
        @trainingref.training_id = @training.id
        @trainingref.save
        format.html { redirect_to @training, notice: 'Training was successfully updated.' }
        format.json { render :show, status: :ok, location: @training }
      else
        puts @training.errors.full_messages
        format.html { render :edit }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trainings/1
  # DELETE /trainings/1.json
  def destroy
    @training = Training.find(params[:id])
    @training.destroy
    respond_to do |format|
      format.html { redirect_to trainings_url, notice: 'Training was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_training
      @training = Training.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def training_params
      params.require(:training).permit(:assoc_skill, :description, :title, :venue, :date, :question_id, :form_id)
    end

  def authorize_admin
    if !current_user.is_admin?
      flash[:error_header] = "Sorry, you are not authorized to view this page."
      flash[:error_body] = "You have been redirected to your profile."
      redirect_to '/profile'
    end
  end

end
