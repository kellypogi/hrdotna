 class FormsQuestionsController < ApplicationController
  before_action :initialize_answered
  def index
    @user = User.find(current_user.id)
    @forms = Form.where("deadline > ?", Date.today)
    @form_groups = FormsQuestion.select("form_id").group(:form_id).distinct
    @form_view = @forms.where(id: @form_groups)
    @answered = current_user.answered
  end
  def show
    @fid = params[:id].to_i
    @formsquestions = FormsQuestion.all

    # @formquestion = FormsQuestion.all
    @questions = Question.all
    @forms = Form.all
    @user = User.find(current_user.id)
    @answers = Answer.all
    # @answer = Answer.new()
    # answer_params = params.require(:formsquestion).permit(:uid, :comp_level, :imp_level)
    # @row = Forms_Question.find(params[:id])

    @ques_in_form = []
    @rows = @formsquestions.where(form_id: @fid)
    @rows.each do |row|
        @ques_in_form.append(row.question_id)
    end

    @useranswers = @answers.where(user_id: current_user.id)
    @skill = @questions.select(:skill).where(id: @ques_in_form).distinct

    @curr_id = params[:page].to_i
    if @curr_id == nil
        @curr_id = 0
    end

    @ques = @questions.where(skill: @skill[@curr_id].skill, id: @ques_in_form).distinct
  end
  def submit
    user = User.find(current_user.id)
    TrainingLog.recommend_(user)
    if user.answered == nil
      user.answered = []
    end
    user.answered.append(params[:fid].to_i)
    user.save
    flash[:success_header] = "Your answers have been submitted!"
    flash[:success_body] =  "The form has been saved to the database."
    redirect_to forms_questions_path
  end
  # def update
  private
    def formsquestion_params
      params.require(:formsquestion).permit(:form_id, :question_id)
    end
    def answer_params
      params.permit(:comp_level, :imp_level)
    end
    def initialize_answered
      if current_user.answered == nil
        current_user.answered = []
      end
      current_user.save
    end
    # @form = Form.find(params[:form_id])
end
