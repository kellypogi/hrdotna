class ProfilesController < ApplicationController
  def index
    @user = User.find(current_user.id)
    @date_before = Date.today-50.year..Date.today
    @trainingsAttended = TrainingLog.joins(:training).where(user_id: current_user.id, recommendation: "Going", trainings: {date: @date_before})
  end
  def edit
    @user = User.find(current_user.id)
  end

  def update
    @user = User.find(current_user.id)
    if @user.update(user_params)
      flash[:success_header] = "Your profile has been updated!"
      flash[:success_body] = "The changes have been saved to the database."
      redirect_to '/profile'
    else
      render 'edit'
    end
  end

  private
  def user_params
    params.require(:user).permit(:full_name, :employee_number, :unit, :designation, :key_function, :classification, :sex, :birth_date, :marital_status, :year, :month)
  end
end
