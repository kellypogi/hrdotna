class AnswersController < ApplicationController
  def index
    @answers = Answer.all
    @users = User.all
    @forms = Form.all
    @forms_questions = FormsQuestion.all
    @questions = Question.all
  end
  def show
    @users = User.all
    @form = Form.find(params[:id])
    @question = Question.find(params[:ques])
    @rows = FormsQuestion.where(form_id: params[:id], question_id: params[:ques])
    @answer = Answer.where(forms_question_id: @rows)
  end
  def new
    @answer = Answer.new
  end

  def create
    @answer = Answer.new
    user = User.find(current_user.id);
    Answer.where(user_id: current_user.id, forms_question_id: params[:fq_id]).first_or_initialize.tap do |a|
      a.comp_level = params[:comp_level]
      a.imp_level = params[:imp_level]
      if a.save!(validate: false)
        respond_to do |format|
          format.json { render :json =>a}
        end
        # TrainingLog.recommend_(user)
      end
    end
  end

  def update
    # user = User.find(current_user.id);
    # if @answer.update(params)
      # TrainingLog.recommend_(user)
    # end
  end

  private
    def answer_params
      params.permit(:comp_level, :imp_level)
    end
end
