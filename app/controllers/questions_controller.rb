class QuestionsController < ApplicationController
  before_action :authorize_admin
  def index
    @questions = Question.all
  end
  def show
    @question = Question.find(params[:id])
  end
  def new
    @question = Question.new
  end
  def edit
    @question = Question.find(params[:id])
  end

  def create
    @question = Question.new(question_params)
    if @question.valid?
      @question.save
      flash[:success_header] = @question.questionDescription + " has been created!"
      flash[:success_body] = "The question has been saved to the database."
      redirect_to questions_path
    else
      render 'new'
    end
  end
  def update
    @question = Question.find(params[:id])
    if @question.update(question_params)
      flash[:success_header] = @question.questionDescription + " has been updated!"
      flash[:success_body] = "The changes have been saved to the database."
      redirect_to @question
    else
      render 'edit'
    end
  end
  def destroy
    @question = Question.find(params[:id])
    @question.destroy
    flash[:success_header] = @question.questionDescription + " has been deleted!"
    flash[:success_body] = "The changes have been saved to the database."
    redirect_to questions_path
  end
  private
    def question_params
      params.require(:question).permit(:questionDescription, :area, :skill)
    end

    def authorize_admin
      if !current_user.is_admin?
        flash[:error_header] = "Sorry, you are not authorized to view this page."
        flash[:error_body] = "You have been redirected to your profile."
        redirect_to '/profile'
      end
    end
end