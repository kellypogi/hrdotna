class RecommendationsController < ApplicationController
	before_action	:authorize_admin
	def index
	end

	private
		def authorize_admin
			if current_user.is_admin?
				redirect_to '/profile'
			end
		end		
end

