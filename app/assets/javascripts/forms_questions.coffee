# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on 'change', ':radio', ->
  b = String($(this).attr('name'))
  a = $('input[name=competence' + $(this).attr('question_id') + ']:checked').val()
  c = $('input[name=importance' + $(this).attr('question_id') + ']:checked').val()

  $.ajax
    type: 'POST'
    url: '/answers'
    dataType: 'json'
    data:
      fq_id: $(this).attr('fq_id')
      comp_level: $('input[name=competence' + $(this).attr('question_id') + ']:checked').val()
      imp_level:   c = $('input[name=importance' + $(this).attr('question_id') + ']:checked').val()
    # success: (data) ->
    #   alert('aha' + (data.comp_level) + 'ehe' + data.imp_level);
    error: (xhr, ajaxOptions, thrownError) ->
      alert(xhr.status)
      alert(thrownError)
  return