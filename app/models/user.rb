class User < ActiveRecord::Base
  has_many :answers, :dependent => :destroy
  has_many :forms_questions, through: :answers
  has_many :training_logs, :class_name => 'TrainingLog'
  has_many :trainings, through: :training_logs
  serialize :answered
  def self.from_omniauth(auth)
    where(email: auth.info.email).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.full_name = auth.info.name
      user.email = auth.info.email
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end

  validates :full_name, presence: :true, length: {maximum: 70}, :on => :update
  validates_format_of :full_name, :with => /\A[a-zA-zñ,. ]*\Z/i, :on => :update
  validates :employee_number, :allow_blank => true, length: {maximum: 11}, :on => :update
  validates_numericality_of :employee_number, :only_integer => true, :allow_blank => true, :greater_than => 0, :message => "is invalid.", :on => :update
  validates :unit, :allow_blank => true, length: {maximum: 70}, :on => :update
  validates_format_of :unit, :with => /\A[a-z0-9A-zñ,.\-() ]*\Z/i, :on => :update
  validates :designation, :allow_blank => true, length: {maximum: 70}, :on => :update
  validates_format_of :designation, :with => /\A[a-z0-9A-zñ,.\-() ]*\Z/i, :on => :update
  validates :key_function, :allow_blank => true, length: {maximum: 70}, :on => :update
  validates_format_of :key_function, :with => /\A[a-z0-9A-zñ,.\-() ]*\Z/i, :on => :update
  validates :marital_status, :allow_blank =>true, inclusion: {in: ['Single', 'Married', 'Separated']}, :on => :update
  validates :classification, :allow_blank =>true, inclusion: {in: ['Faculty', 'Reps', 'Admin']}, :on => :update
  validates :birth_date, date: {allow_blank: true, after: Date.today - 100.year, before: Date.today - 10.year}, :on => :update
  validates :sex, :allow_blank =>true, inclusion: {in: ['Male', 'Female']}, :on => :update
  validates_numericality_of :year, :only_integer => true, :allow_blank => true, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100, :message => "is invalid.", :on => :update
  validates_numericality_of :month, :only_interger => true, :allow_blank => true, :greater_than_or_equal_to => 0, :less_than => 12, :message => "is invalid.", :on => :update

end
