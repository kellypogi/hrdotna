class Training < ApplicationRecord
    #has_one :questions
    #has_one :forms
    validates :title, presence: :true
    validates :title, length: {maximum: 100}
    has_many :training_logs, :class_name => 'TrainingLog'
    has_many :users, through: :training_logs
    belongs_to :form #, optional: true
    belongs_to :question #, optional: true

    #instance methods
    def set_form(form_id) #sets the associated form
        self.form_id = form_id
    end
end
