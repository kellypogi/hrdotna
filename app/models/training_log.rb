class TrainingLog < ApplicationRecord
  belongs_to :user
  belongs_to :training

  def self.recommend      #for batch runs
    users = User.all #path 1
    users.each do |user|  #path 2
      answers = Answer.where(imp_level: 4, comp_level:1, user_id: user.id) #path 3,    gets the each user's answers with the right criteria
      answers.each do |answer|  #path 4
        forms_question = answer.forms_question    #path 5
        form = forms_question.form                                 #finds where the form belongs
        training = Training.joins(:form).where(form_id: form.id, id: form.training_id, question_id: forms_question.question_id).first
        if training != nil                #path 6                  #do nothing if theres no training for the form yet
          recommendation = TrainingLog.where(user_id: user.id, training_id: training.id).first_or_create(recommendation: 'Recommended') #if the recommendation doesnt exist yet, create it
          recommendation.save           #path 7
        end     #this implementation doesnt consider that required skill. Filter the form questions with the required skill first
      end
    end
  end
  def self.recommend_(user)   #individual suggestions

    answers = Answer.where(imp_level: 4, comp_level:1, user_id: user.id) #path 3,    gets the each user's answers with the right criteria
    answers.each do |answer|  #path 4
      forms_question = answer.forms_question    #path 5
      form = forms_question.form                                 #finds where the form belongs
                      #training = Form.joins(:training).where(id: form.id, training_id: form.training_id, :trainings => {question_id: forms_question.question_id}).first #gets the training associated with the form
      training = Training.joins(:form).where(form_id: form.id, question_id: forms_question.question_id).first
      if training != nil                #path 6                  #do nothing if theres no training for the form yet
        recommendation = TrainingLog.where(user_id: user.id, training_id: training.id).first_or_create(recommendation: 'Recommended') #if the recommendation doesnt exist yet, create it
        recommendation.save           #path 7
        puts recommendation.errors.full_messages
      end     #this implementation doesnt consider that required skill. Filter the form questions with the required skill first
    end
  end
end
