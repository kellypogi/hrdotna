class Form < ApplicationRecord
  has_many :forms_questions, :dependent => :destroy
  has_many :questions, through: :forms_questions
  has_one :training
  validates :title, presence: true
  validates :deadline, presence: true
end
