class FormsQuestion < ApplicationRecord
  belongs_to :form
  belongs_to :question
  has_many :answers, :dependent => :destroy
  has_many :users, through: :answers
#  validates :skill, presence: true
  validates_presence_of :form
end
