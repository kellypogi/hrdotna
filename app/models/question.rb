class Question < ApplicationRecord
    has_many :forms_questions, :dependent => :destroy
    has_many :forms, through: :forms_questions
    has_many :trainings
    validates :questionDescription, presence: true
    validates :skill, presence: true
end
