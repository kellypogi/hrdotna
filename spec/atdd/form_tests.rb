require 'rails_helper'
require 'watir'

feature "User" do
	scenario "Answers a form" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		browser.link(:id, "sf").click
		browser.a(:href, "/forms_questions/6").click
		browser.radio(:id, "comp_level3_6").set
		browser.radio(:id, "imp_level4_6").set
		browser.button(:id, "next").click
		browser.radio(:id, "comp_level4_9").set
		browser.radio(:id, "imp_level3_9").set
		browser.radio(:id, "comp_level3_10").set
		browser.radio(:id, "imp_level3_10").set
		browser.button(:id, "next").click
		browser.radio(:id, "comp_level3_16").set
		browser.radio(:id, "imp_level2_16").set
		browser.radio(:id, "comp_level4_17").set
		browser.radio(:id, "imp_level2_17").set
		browser.button(:id, "next").click
		browser.radio(:id, "comp_level3_30").set
		browser.radio(:id, "imp_level2_30").set
		browser.radio(:id, "comp_level1_31").set
		browser.radio(:id, "imp_level1_31").set
		browser.link(:text, "Submit").click
		Watir::Wait.until(60) { browser.text.include? "Your answers have been submitted!" }
		browser.text.should include("Your answers have been submitted!")
		browser.close
	end
	scenario "Answers existing form" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		browser.link(:id, "sf").click
		browser.a(:href, "/forms_questions/6").click
		browser.radio(:id, "comp_level4_6").set
		browser.radio(:id, "imp_level1_6").set
		browser.button(:id, "next").click
		browser.radio(:id, "comp_level1_9").set
		browser.radio(:id, "imp_level2_9").set
		browser.radio(:id, "comp_level2_10").set
		browser.radio(:id, "imp_level4_10").set
		browser.button(:id, "next").click
		browser.radio(:id, "comp_level4_16").set
		browser.radio(:id, "imp_level1_16").set
		browser.radio(:id, "comp_level3_17").set
		browser.radio(:id, "imp_level4_17").set
		browser.button(:id, "next").click
		browser.radio(:id, "comp_level2_30").set
		browser.radio(:id, "imp_level1_30").set
		browser.radio(:id, "comp_level3_31").set
		browser.radio(:id, "imp_level4_31").set
		browser.link(:text, "Submit").click
		Watir::Wait.until(60) { browser.text.include? "Your answers have been submitted!" }
		browser.text.should include("Your answers have been submitted!")
		browser.close
	end
end

feature "Admin" do
	scenario "Creates a form" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.link(:id, "f").click
		browser.link(:id, "red_button").click
		browser.text_field(:id, "form_title").set("New Test Form")
		browser.text_field(:id, "form_description").set("This is a new test form to be added for ATDD.")
		browser.text_field(:id, "form_deadline").set("2017-09-20")
		browser.checkbox(:id, "selectAll").set
		browser.button(:id, "red_button").click
		Watir::Wait.until(60) { browser.text.include? "New Test Form has been created!" }
		browser.text.should include("New Test Form has been created!")
		browser.text.should include("New Test Form")
		browser.text.should include("This is a new test form to be added for ATDD.")
		browser.text.should include("Sep 20, 2017")
		browser.close
	end
	scenario "Views a form" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.link(:id, "f").click
		browser.goto("http://localhost:3000/forms/13")
		Watir::Wait.until(60) { browser.text.include? "Form Information" }
		browser.text.should include("New Test Form")
		browser.text.should include("This is a new test form to be added for ATDD.")
		browser.text.should include("Sep 20, 2017")
		browser.text.should include("Communication Management")
		browser.text.should include("Organizational & Leadership Management Skills")
		browser.text.should include("Functional Skills")
		browser.text.should include("System Information and Technology")
		browser.text.should include("Service Skills")
		browser.close	
	end
	scenario "Updates a form" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.link(:id, "f").click
		browser.goto("http://localhost:3000/forms/13")
		Watir::Wait.until(60) { browser.text.include? "Form Information" }
		browser.link(:id, "edit").click
		browser.text_field(:id, "form_title").set("Edited New Test Form")
		browser.text_field(:id, "form_description").set("This is a new edited test form to be added for ATDD.")
	 	browser.text_field(:id, "form_deadline").set("2017-10-12")
	 	browser.button(:id, "red_button").click
	 	Watir::Wait.until(60) { browser.text.include? "Edited New Test Form has been updated!" }
	 	browser.text.should include("Edited New Test Form has been updated!")
	 	browser.text.should include("Edited New Test Form")
		browser.text.should include("This is a new edited test form to be added for ATDD.")
		browser.text.should include("Oct 12, 2017")
		browser.close
	end	
	scenario "Deletes a form" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.link(:id, "f").click
		browser.goto("http://localhost:3000/forms/13")
		Watir::Wait.until(60) { browser.text.include? "Form Information" }
		browser.link(:id, "delete").click
		browser.execute_script("window.confirm = function() {return true}")
		browser.alert.ok
		Watir::Wait.until(60) { browser.text.include? "Edited New Test Form has been deleted!" }
		browser.text.should include("Edited New Test Form has been deleted!")
		browser.close
	end
end