require 'rails_helper'
require 'watir'

feature "Answer list" do
  scenario "View answers" do
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:id, "ea").click
    browser.text.should include("Ninja Skills Form")
    browser.div(:text, "Ninja Skills Form").click
    browser.div(:text, "Rasengan Ability").click
    browser.text.should include("Competence")
    browser.text.should include("Kelly Navarro")
    browser.close
  end
end
