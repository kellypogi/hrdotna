require 'rails_helper'
require 'watir'

feature "Login from Google" do
	scenario "User test" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.text.should include("My Profile")
		browser.text.should_not include("Admin Account")
		browser.close
	end
	scenario "Admin test" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.text.should include("My Profile")
		browser.text.should include("Admin Account")
		browser.close
	end
	scenario "Validate account" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.text.should include("My Profile")
		browser.text.should include("Admin Account")
		browser.text.should include("HRDO TNA")
		browser.close
	end
end

feature "User tests" do
	scenario "Answers a form" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		browser.link(:id, "sf").click
		browser.a(:href, "/forms_questions/5").click
		browser.radio(:id, "comp_level4_5").set
		browser.radio(:id, "imp_level2_5").set
		browser.radio(:id, "comp_level2_6").set
		browser.radio(:id, "imp_level1_6").set
		browser.button(:id, "next").click
		browser.radio(:id, "comp_level3_11").set
		browser.radio(:id, "imp_level2_11").set
		browser.radio(:id, "comp_level2_12").set
		browser.radio(:id, "imp_level1_12").set
		browser.button(:id,"next").click
		browser.radio(:id, "comp_level4_21").set
		browser.radio(:id, "imp_level3_21").set
		browser.radio(:id, "comp_level2_22").set
		browser.radio(:id, "imp_level4_22").set
		browser.link(:text, "Submit").click
		browser.text.should include("Your answers have been submitted!")
		browser.close
	end
	scenario "View profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
 		browser.text.should include("My Profile")
		browser.text.should include("Mae")
		browser.close
	end
	scenario "Edit profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
 		browser.text.should include("My Profile")
		browser.text.should include("Mae")
		browser.link(:text, "Edit Profile").click
		browser.text_field(:id, "user_employee_number").set("192192129")
		browser.text_field(:id, "user_unit").set("OVCSAA")
		browser.text_field(:id, "user_designation").set("Supervisor")
		browser.select_list(:id, "user_marital_status").select("Single")
		browser.text_field(:id, "user_birth_date").set("1997-03-08")
		browser.select_list(:id, "user_sex").select("Female")
		browser.text_field(:id, "user_year").set("6")
		browser.text_field(:id, "user_month").set("2")
		browser.link(:text,"Submit").click
		browser.text.should include("Your profile has been updated!")
		browser.text.should include("192192129")
		browser.text.should include("OVCSAA")
		browser.text.should include("Supervisor")
		browser.text.should include("Single")
		browser.text.should include("Mar 08, 1997")
		browser.text.should include("Female")
		browser.text.should include("6 years and 2 months")
		browser.close
	end
end

feature "Admin tests" do
	scenario "View profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.text.should include("My Profile")
		browser.text.should include("Admin Account")		
		browser.text.should include("HRDO TNA")
		browser.close
	end
	scenario "Edit profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.text.should include("My Profile")
		browser.text.should include("Admin Account")		
		browser.text.should include("HRDO TNA")
		browser.link(:text, "Edit Profile").click
		browser.text_field(:id, "user_employee_number").set("291291291")
		browser.text_field(:id, "user_unit").set("HRDO")
		browser.text_field(:id, "user_designation").set("Dummy")
		browser.select_list(:id, "user_marital_status").select("Single")
		browser.text_field(:id, "user_birth_date").set("1967-05-02")
		browser.select_list(:id, "user_sex").select("Male")
		browser.text_field(:id, "user_year").set("3")
		browser.text_field(:id, "user_month").set("4")
		browser.link(:text,"Submit").click
		browser.button(:value,"Submit").click
		browser.text.should include("Your profile has been updated!")
		browser.text.should include("291291291")
		browser.text.should include("HRDO")
		browser.text.should include("Dummy")
		browser.text.should include("Single")
		browser.text.should include("May 02, 1967")
		browser.text.should include("Male")
		browser.text.should include("3 years and 4 months")
		browser.close
	end
	scenario "View all employees" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		browser.link(:id, "el").click
		browser.text.should include("Employee List")
		browser.text.should include("Aguinaldo, Maria Berlinda G.")
		browser.text.should include("Allan C. Esdartero")
		browser.text.should include("Alvaniz, Carmen B.")
		browser.text.should include("Amigo, Renell")
		browser.text.should include("Antonio M Baldoza")
		browser.text.should include("Arnulfo Marquezo")
		browser.text.should include("Auxilian, Rosita C.")
		browser.text.should include("Baena, Nanette S.")
		browser.text.should include("Bartolabac, Marius")
		browser.text.should include("Bautista, Jasmine V.")
		browser.close
	end
	scenario "Search for employees" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		browser.link(:id, "el").click
		browser.text_field(:type, "search").set("Zuniga")
		browser.link(:text, "Zuniga, Philip Christian").click
		browser.text.should include("Zuniga, Philip Christian")
		browser.text.should include("261963140")
		browser.close
	end
end
