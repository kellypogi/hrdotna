require 'rails_helper'
require 'watir'

feature "User" do
	scenario "Views profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
 		browser.text.should include("My Profile")
		browser.text.should include("Mae")
		browser.close
	end
	scenario "Edits profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("jamae43@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("password34")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
 		browser.text.should include("My Profile")
		browser.text.should include("Mae")
		browser.link(:text, "Edit Profile").click
		browser.text_field(:id, "user_employee_number").set("192192129")
		browser.text_field(:id, "user_unit").set("OVCSAA")
		browser.text_field(:id, "user_designation").set("Supervisor")
		browser.select_list(:id, "user_marital_status").select("Single")
		browser.text_field(:id, "user_birth_date").set("1997-03-08")
		browser.select_list(:id, "user_sex").select("Female")
		browser.text_field(:id, "user_year").set("6")
		browser.text_field(:id, "user_month").set("2")
		browser.button(:id, "red_button").click
		Watir::Wait.until(60) { browser.text.include? "Your profile has been updated!" }
		browser.text.should include("Your profile has been updated!")
		browser.text.should include("192192129")
		browser.text.should include("OVCSAA")
		browser.text.should include("Supervisor")
		browser.text.should include("Single")
		browser.text.should include("Mar 08, 1997")
		browser.text.should include("Female")
		browser.text.should include("6 years and 2 months")
		browser.close
	end
end

feature "Admin" do
	scenario "Views profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.text.should include("My Profile")
		browser.text.should include("Admin Account")		
		browser.text.should include("HRDO TNA")
		browser.close
	end
	scenario "Edits profile" do
		browser = Watir::Browser.new :firefox
		browser.goto("http://localhost:3000/")
		browser.link(:id, "red_button").click
		browser.text_field(:id, "Email").set("hrdotna.dummy@gmail.com")
		browser.button(:value,"Next").click
		browser.text_field(:id, "Passwd").set("ilovecs192")
		browser.button(:value,"Sign in").click
		browser.button(:value,"Allow").click
		Watir::Wait.until(60) { browser.text.include? "My Profile" }
		browser.text.should include("My Profile")
		browser.text.should include("Admin Account")		
		browser.text.should include("HRDO TNA")
		browser.link(:text, "Edit Profile").click
		browser.text_field(:id, "user_employee_number").set("291291291")
		browser.text_field(:id, "user_unit").set("HRDO")
		browser.text_field(:id, "user_designation").set("Dummy")
		browser.select_list(:id, "user_marital_status").select("Single")
		browser.text_field(:id, "user_birth_date").set("1967-05-02")
		browser.select_list(:id, "user_sex").select("Female")
		browser.text_field(:id, "user_year").set("3")
		browser.text_field(:id, "user_month").set("4")
		browser.button(:id, "red_button").click
		Watir::Wait.until(60) { browser.text.include? "Your profile has been updated!" }
		browser.text.should include("Your profile has been updated!")
		browser.text.should include("291291291")
		browser.text.should include("HRDO")
		browser.text.should include("Dummy")
		browser.text.should include("Single")
		browser.text.should include("May 02, 1967")
		browser.text.should include("Female")
		browser.text.should include("3 years and 4 months")
		browser.close
	end
end