require 'rails_helper'
require 'watir'

feature "Training" do
  scenario "Views trainings" do
    # browser = Watir::Browser.start  "http://localhost:3000/"
    # browser.link(:id, "red_button").click
    # browser.text_field(:id, "Email").set("gabrnavarro1@gmail.com")
    # browser.button(:id, "next").click
    # browser.text_field(:id, "Passwd").set("rarevolcano")
    # browser.checkbox(:id, "PersistentCookie").clear
    # browser.button(:id, "signIn").click
    # browser.button(:id, "submit_approve_access").click
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:id, "t").click
    browser.text.should include("Trainings")
    browser.link(:id, "red_button").click
    browser.text.should include("New Training")
    browser.text_field(:id, "training_title").set("The Training")
    browser.text_field(:id, "training_description").set("This is a test training created.")
    browser.text_field(:id, "training_venue").set("Palma Hall")
    browser.text_field(:id, "training_date").set("2017-05-05")
    browser.select_list(:id, "training_question_id").select("Rasengan Ability")
    browser.button(:id, "red_button").click
    Watir::Wait.until(60){ browser.text.include? "The Training"}
    browser.text.should include("The Training")
    browser.close
  end
  scenario "Edits training" do
    # browser = Watir::Browser.start  "http://localhost:3000/"
    # browser.link(:id, "red_button").click
    # browser.text_field(:id, "Email").set("gabrnavarro1@gmail.com")
    # browser.button(:id, "next").click
    # browser.text_field(:id, "Passwd").set("rarevolcano")
    # browser.checkbox(:id, "PersistentCookie").clear
    # browser.button(:id, "signIn").click
    # browser.button(:id, "submit_approve_access").click
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:id, "t").click
    browser.text.should include("The Training")
    browser.link(:text, "The Training").click
    browser.text.should include("Training Information")
    browser.link(:id, "red_button").click
    browser.text_field(:id, "training_title").set("The New Training")
    browser.text_field(:id, "training_venue").set("Melchor Hall")
    browser.text_field(:id, "training_date").set("2019-05-05")
    browser.button(:id, "red_button").click
    Watir::Wait.until(60){ browser.text.include? "The New Training"}
    browser.text.should include("The New Training")
    browser.close
  end
  scenario "Deletes Training" do
    # browser = Watir::Browser.start  "http://localhost:3000/"
    # browser.link(:id, "red_button").click
    # browser.text_field(:id, "Email").set("gabrnavarro1@gmail.com")
    # browser.button(:id, "next").click
    # browser.text_field(:id, "Passwd").set("rarevolcano")
    # browser.checkbox(:id, "PersistentCookie").clear
    # browser.button(:id, "signIn").click
    # browser.button(:id, "submit_approve_access").click
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:id, "t").click
    browser.text.should include("The New Training")
    browser.link(:text, "The New Training").click
    browser.link(:text, "Delete").click
    browser.close
  end
  scenario "Views recommended employees" do
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:id, "t").click
    browser.text.should include("Chuunin Training")
    browser.link(:text, "Chuunin Training").click
    browser.text.should include("Training Information")
    browser.text.should include("Mae")
    browser.close
  end
  scenario "Views Recommended List tab" do
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:id, "t").click
    browser.text.should include("Chuunin Training")
    browser.link(:text, "Chuunin Training").click
    browser.text.should include("Training Information")
    browser.link(:text, "Recommended List").click
    browser.text.should include("Recommended Employee")
    browser.close
  end
end
