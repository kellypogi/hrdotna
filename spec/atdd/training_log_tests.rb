require 'rails_helper'
require 'watir'


feature "TrainingLog" do
  scenario "Employee Views Invitations" do
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("jamae43@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("password34")
    browser.send_keys :enter
    browser.link(:id, "t").click
    browser.text.should include("Chuunin Training")
  end
end
