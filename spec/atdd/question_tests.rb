require 'rails_helper'
require 'watir'

feature "Questions" do
  scenario "creates question" do
    # browser = Watir::Browser.start  "http://localhost:3000/"
    # browser.link(:id, "red_button").click
    # browser.text_field(:id, "Email").set("gabrnavarro1@gmail.com")
    # browser.button(:id, "next").click
    # browser.text_field(:id, "Passwd").set("rarevolcano")
    # browser.checkbox(:id, "PersistentCookie").clear
    # browser.button(:id, "signIn").click
    # browser.button(:id, "submit_approve_access").click
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:text, "Question Database").click
    browser.link(:id, "red_button").click
    browser.text_field(:id, "question_questionDescription").set("Kung Fu Ability")
    browser.text_field(:id, "question_skill").set("Fighting Skills")
    browser.button(:id, "red_button").click
    Watir::Wait.until(60){ browser.text.include? "Kung Fu Ability"}
    browser.text.should include("Kung Fu Ability")
  end
  scenario "View question" do
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    browser.link(:text, "Question Database").click
    browser.link(:text, "Kung Fu Ability").click
    browser.text.should include("Kung Fu Ability")
  end
end
