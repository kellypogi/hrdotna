require 'rails_helper'
require 'watir'

feature "User List" do
  scenario "Views Users" do
    # browser = Watir::Browser.start  "http://localhost:3000/"
    # browser.link(:id, "red_button").click
    # browser.text_field(:id, "Email").set("gabrnavarro1@gmail.com")
    # browser.button(:id, "next").click
    # browser.text_field(:id, "Passwd").set("rarevolcano")
    # browser.checkbox(:id, "PersistentCookie").clear
    # browser.button(:id, "signIn").click
    # browser.button(:id, "submit_approve_access").click
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "My Profile" }
    browser.link(:id, "el").click
    Watir::Wait.until(10) { browser.text.include? "Bautista, Jasmine V." }
    browser.link(:text, "Bautista, Jasmine V.").click
    browser.text.should include("Bautista, Jasmine V.")
    browser.close
  end
  scenario "Sets as admin" do
    # browser = Watir::Browser.start  "http://localhost:3000/"
    # browser.link(:id, "red_button").click
    # browser.text_field(:id, "Email").set("gabrnavarro1@gmail.com")
    # browser.button(:id, "next").click
    # browser.text_field(:id, "Passwd").set("rarevolcano")
    # browser.checkbox(:id, "PersistentCookie").clear
    # browser.button(:id, "signIn").click
    # browser.button(:id, "submit_approve_access").click
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "My Profile" }
    browser.link(:id, "el").click
    Watir::Wait.until(10) { browser.text.include? "Bautista, Jasmine V." }
    browser.link(:text, "Bautista, Jasmine V.").click
    browser.text.should_not include("Remove as Admin")
    browser.button(:id, "red_button").click
    browser.text.should include("Remove as Admin")
    browser.button(:name, "user_id").click
    browser.text.should_not include("Remove as Admin")
    browser.close
  end
  scenario "searches for users" do
    browser = Watir::Browser.start  "http://localhost:3000/"
    browser.link(:id, "red_button").click
    browser.text_field(:id, "identifierId").set("gabrnavarro1@gmail.com")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "Enter your password" }
    browser.text_field(:type, "password").set("rarevolcano")
    browser.send_keys :enter
    Watir::Wait.until(60) { browser.text.include? "My Profile" }
    browser.link(:id, "el").click
    Watir::Wait.until(10) { browser.text.include? "SEARCH:" }
    browser.text_field.set("Zuniga")
    Watir::Wait.until(2) { browser.text.include? "Zuniga" }
    browser.text.should include("Zuniga")
    browser.close
  end
end
