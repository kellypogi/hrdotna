FactoryGirl.define do
  factory :form do |f|
    f.title "Sample Form 1"
    f.created_at Faker::Date.backward(14)
    f.updated_at Faker::Date.backward(14)
    f.status "Ongoing"
    f.deadline Faker::Date.forward(23)
    f.description Faker::Lorem.sentence
  end
  factory :user do |u|
  	u.provider "google_oauth2"
  	u.uid 12345678
  	u.name "Jaye Hernandez"
  	u.oauth_token "token"
  	u.oauth_expires_at 1354920555
  	u.created_at Faker::Date.backward(14)
  	u.updated_at Faker::Date.backward(14)
  	u.date_stamp Faker::Date.backward(14)
  	u.tna_stamp "stamp"
  	u.employee_number Faker::Number.number(8)
  	u.full_name "Jaye Hernandez"
  	u.birth_date Faker::Date.birthday(18, 65)
  	u.sex "Male"
  	u.marital_status "Single"
  	u.unit "Unit"
  	u.designation "Designation"
  	u.year "4"
  	u.is_admin true
  	u.email Faker::Internet.free_email
    u.answered nil
  end
  factory :training do |t|
  	t.created_at Faker::Date.backward(14)
  	t.updated_at Faker::Date.backward(14)
  	t.title "Training 1"
  	t.assoc_skill "Computer"
  	t.date Faker::Date.forward(23)
  	t.description Faker::Lorem.sentence
  	t.venue "Department of Computer Science"
  	t.status "Open"
    t.question_id 1
    form
    question
  end
  factory :question do |q|
    q.questionDescription Faker::Lorem.sentence
    q.area "Soft Skills"
    q.skill "Communication"
    q.created_at Faker::Date.backward(14)
    q.updated_at Faker::Date.backward(14)
  end
  factory :forms_question do |f|
    f.form_id 1
    f.question_id 1
    f.area "Soft Skills"
    f.skill "Communication"
    f.created_at Faker::Date.backward(14)
    f.updated_at Faker::Date.backward(14)
    form
    question
  end
  factory :answer do |a|
    a.comp_level "3"
    a.imp_level "3"
    a.user_id 1
    a.forms_question_id 1
    a.created_at Faker::Date.backward(14)
    a.updated_at Faker::Date.backward(14)
    user
    forms_question
  end
  factory :training_log do |t|
    t.recommendation "recommended"
    t.created_at Faker::Date.backward(14)
    t.updated_at Faker::Date.backward(14)
    t.user_id 1
    t.training_id 1
    training
    user
  end
end
