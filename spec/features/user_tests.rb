require 'rails_helper'

def stub_omniauth
  # first, set OmniAuth to run in test mode
  OmniAuth.config.test_mode = true
  # then, provide a set of fake oauth data that
  # omniauth will use when a user tries to authenticate:
  OmniAuth.config.mock_auth[:google] = OmniAuth::AuthHash.new({
      provider: "google",
      uid: "112010221912200065887",
      info: {
        email: "hernandez.jaye@gmail.com",
        first_name: "Jaye",
        last_name: "Hernandez"
      }
  })
end

RSpec.feature "user logs in" do
  scenario "using google login" do
    stub_omniauth
    visit root_path
    expect(page).to have_link("Sign in with Google")
    click_link "Sign in with Google"
    expect(page).to have_content("Employee Profile")
  end
end

RSpec.feature "user uses survey forms" do
  scenario "views survey forms" do
    visit "/profile"
    expect(page).to have_link("Survey Forms")
    click_link "Survey Forms"
    expect(page).to have_link("Survey Forms")
  end
end