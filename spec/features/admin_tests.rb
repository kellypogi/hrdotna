require 'rails_helper'

def stub_omniauth
  # first, set OmniAuth to run in test mode
  OmniAuth.config.test_mode = true
  # then, provide a set of fake oauth data that
  # omniauth will use when a user tries to authenticate:
  OmniAuth.config.mock_auth[:google] = OmniAuth::AuthHash.new({
      provider: "google",
      uid: "112010221912200065887",
      info: {
        email: "hernandez.jaye@gmail.com",
        first_name: "Jaye",
        last_name: "Hernandez",
        is_admin: true
      }
  })
end

RSpec.feature "admin logs in" do
  scenario "using google login" do
    stub_omniauth
    visit root_path
    expect(page).to have_link("Sign in with Google")
    click_link "Sign in with Google"
    expect(page).to have_content("Employee Profile")
  end
end

################################################################
RSpec.feature "admin uses employee list" do
  scenario "view one employee" do
    visit "/profile"
    expect(page).to have_content("Employee List")
    click_link "Employee List"
    expect(page).to have_content("Employee List")
    expect(page).to have_content("Silleza, Robert G.")
    click_link "Silleza, Robert G."
    expect(page).to have_content("Employee Profile: Silleza, Robert G.")
  end
  scenario "search for an employee" do
    visit "/profile"
    click_link "Employee List"
    expect(page).to have_content("Employee List")
  end
end

#############################################
RSpec.feature "admin access forms" do
  before(:each) do
    load "#{Rails.root}/db/test.rb"
  end

  scenario "admin clicks forms in side menu" do
    visit "/profile"
    expect(page).to have_content("Employee Profile")
    click_link "Survey Forms"
    expect(page).to have_content("Forms")
  end

  scenario "Display list of forms" do
    visit "/forms"
    expect(page).to have_content("Forms")
    expect(page).to have_content("Form Title")
    expect(page).to have_content("Description")
    expect(page).to have_content("Status")
    expect(page).to have_content("Date Added")
    expect(page).to have_content("Deadline")
    expect(page).to have_content("Temp Form 1")
  end

  scenario "Click and Display Form Content" do
    visit "/forms"
    click_link "Temp Form 1"
    expect(page).to have_content("Form Information")
    expect(page).to have_content("Description")
    expect(page).to have_content("Status")
    expect(page).to have_content("Ongoing")
    expect(page).to have_content("Deadline")
    expect(page).to have_content("Questions Included")
    #save_and_open_page
  end

  scenario "edit button leads to edit page" do
    visit "/forms"
    click_link "Temp Form 1"
    click_link "Edit Form"
    expect(page).to have_content("Edit Form: Temp Form 1")
    fill_in "form_title", with: 'sample form', visible: false
    fill_in "form_description", with: 'sample description', visible: false
    fill_in "form_deadline", with: '03/18/2017', visible: false

    #expect(page).to have_content("sample description")
    expect(page).to have_button('Submit')
    click_button "Submit"
    check('check')
    save_and_open_page
  end
  scenario "HEY" do
    visit current_path
  end

end

#########################################
RSpec.feature "admin access Questions list" do
  before(:each) do
    load "#{Rails.root}/db/test.rb"
  end

  scenario "admin clicks question database in side menu" do
    visit "/profile"
    expect(page).to have_content("Question Database")
    click_link "Question Database"
    expect(page).to have_content("Questions")
  end
end

#########################################
RSpec.feature "admin access recommend trainings" do
  before(:each) do
    load "#{Rails.root}/db/test.rb"
  end

  scenario "admin clicks recommended trainings in side menu" do
    visit "/profile"
    expect(page).to have_content("Employee Profile")
    click_link "Recommend Trainings"
    expect(page).to have_content("Recommend Trainings")
  end
end
