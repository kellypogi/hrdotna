require 'rails_helper'

describe "Submitted Form View Test" do
  it "form can't be viewed by user if form is submitted" do
    user = FactoryGirl.create(:user)
    user.update(answered: [])
    expect(user.answered).to eq([])

    form = FactoryGirl.create(:form)
    form.update(deadline: Date.today+3.month)
    expect(form.deadline).to eq(Date.today+3.month)

    ques = FactoryGirl.create(:question)
    form_ques = FactoryGirl.create(:forms_question)
    form_ques.update(form_id: form.id, question_id:ques.id)
    expect(form_ques.form_id).to eq(form.id)
    expect(form_ques.question_id).to eq(ques.id)

    forms = Form.where("deadline > ?", Date.today)
    form_groups = FormsQuestion.select("form_id").group(:form_id).distinct
    form_view = forms.where(id: form_groups)
    f = form_view.where(id: form.id).first
    expect(f.id).to eq(form.id)

    user.answered.append(f.id)
    user.save

    disable = false
    form_view.each do |f|
      if user.answered.include? f.id
        disable = true
      end
    end

    expect(disable).to eq(true)
  end

  it "form can be viewed by user if form is not yet submitted" do
    user = FactoryGirl.create(:user)
    user.update(answered: [])
    expect(user.answered).to eq([])

    form = FactoryGirl.create(:form)
    form.update(deadline: Date.today+3.month)
    expect(form.deadline).to eq(Date.today+3.month)

    ques = FactoryGirl.create(:question)
    form_ques = FactoryGirl.create(:forms_question)
    form_ques.update(form_id: form.id, question_id:ques.id)
    expect(form_ques.form_id).to eq(form.id)
    expect(form_ques.question_id).to eq(ques.id)

    forms = Form.where("deadline > ?", Date.today)
    form_groups = FormsQuestion.select("form_id").group(:form_id).distinct
    form_view = forms.where(id: form_groups)
    f = form_view.where(id: form.id).first
    expect(f.id).to eq(form.id)

    disable = false
    form_view.each do |f|
      if user.answered.include? f.id
        disable = true
      end
    end

    expect(disable).to eq(false)
  end

end
