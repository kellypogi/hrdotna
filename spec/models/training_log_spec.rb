require 'rails_helper'

RSpec.describe TrainingLog, type: :model do
  describe "Basis Path Testing" do
    it "no users in the database" do # run rails db:test: prepare to pass
      users = User.all
      expect(users).to be_empty
    end
    it "no answers in the database" do
      u1 = FactoryGirl.create(:user)
      users = User.all
      expect(users).to include(u1)
      users.each do |user|
        answers = Answer.where(imp_level: 4, comp_level:1, user_id: user.id)
        expect(answers).to be_empty
      end
    end
    it "answers exist, but there is no assigned training to the form" do
      u1 = FactoryGirl.create(:user)
      f = FactoryGirl.create(:form, training_id: nil)
      q = FactoryGirl.create(:question)
      fq = FactoryGirl.create(:forms_question, form_id: f.id, question_id: q.id)
      a = FactoryGirl.create(:answer, forms_question_id: fq.id, user_id: u1.id)
      t = FactoryGirl.create(:training)
      users = User.all
      expect(users).to include(u1)
      users.each do |user|
        answers = Answer.where(imp_level: 4, comp_level:1, user_id: user.id)
        expect(answers).to_not be_nil
        answers.each do |answer|
          expect(answer.imp_level).to eq("4")
          expect(answer.comp_level).to eq("1")
          expect(answer.user_id).to eq(user.id.to_s)
          forms_question = answer.forms_question
          expect(forms_question).to_not be_nil
          expect(forms_question).to eq(fq)
          form = forms_question.form
          expect(form).to_not be_nil
          expect(form).to eq(f)
          training = Form.joins(:training).where(id: form.id, training_id: form.training_id).first
          expect(training).to be_nil
        end
      end
    end
    it "answers exist, form has assigned training" do
      u1 = FactoryGirl.create(:user)
      f = FactoryGirl.create(:form, training_id: nil)
      q = FactoryGirl.create(:question)
      fq = FactoryGirl.create(:forms_question, form_id: f.id, question_id: q.id)
      a = FactoryGirl.create(:answer, forms_question_id: fq.id, user_id: u1.id)
      t = FactoryGirl.create(:training)
      users = User.all
      expect(users).to include(u1)
      users.each do |user|
        answers = Answer.where(imp_level: 4, comp_level:1, user_id: user.id)
        expect(answers).to_not be_nil
        answers.each do |answer|
          expect(answer.imp_level).to eq("4")
          expect(answer.comp_level).to eq("1")
          expect(answer.user_id).to eq(user.id.to_s)
          forms_question = answer.forms_question
          expect(forms_question).to_not be_nil
          expect(forms_question).to eq(fq)
          form = forms_question.form
          expect(form).to_not be_nil
          expect(form).to eq(f)
          training = Form.joins(:training).where(id: form.id, training_id: form.training_id).first
          expect(training).to_not be_nil
          if training != nil
            reco = TrainingLog.where(user_id: user.id, training_id: training.id, recommendation: 'recommended').first_or_create()
            reco.save
            expect(reco).to_not be_nil
            expect(reco.recommendation).to eq("recommended")
          end
        end
      end
    end
    it "saves a recommendation when training is not nil" do
      training = FactoryGirl.create(:training)
      expect(training).to_not be_nil
      if training != nil
        reco = true
        expect(reco).to_not be_nil
      end
    end
    it "doesnt save when training is nil" do
      training = nil
      reco = nil
      if training != nil
        reco = TrainingLog.where(user_id: user.id, training_id: training.id, recommendation: 'recommended').first_or_create()
        reco.save
      end
      expect(reco).to be_nil
    end
  end # end describe basis path
  describe "Validations" do
    before(:all) do #setup factories first
      @u1 = FactoryGirl.create(:user)
      @t1 = FactoryGirl.create(:training)
    end
    it "is a valid training log with valid user and trainings" do
      expect(@u1).to be_valid
      expect(@t1).to be_valid
      tl1 = TrainingLog.new(user_id: @u1.id, training_id: @t1.id)
      expect(tl1).to be_valid
    end
    it "is not a valid training log without a valid user" do
      @u1.id = nil
      tl1 = TrainingLog.new(user_id: @u1.id, training_id: @t1.id)
      expect(tl1).to_not be_valid
    end
    it "is not a valid training log without a valid training" do
      @t1.id = nil
      tl1 = TrainingLog.new(user_id: @u1.id, training_id: @t1.id)
      expect(tl1).to_not be_valid
    end
  end
end
