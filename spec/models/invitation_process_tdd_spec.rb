require 'rails_helper'

describe "Invitation Process" do
  it "user is recommended to a training" do
    u1 = FactoryGirl.create(:user)
    t = FactoryGirl.create(:training)
    reco = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended').first_or_create()

    t_log = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended')
    expect(t_log).to_not be_empty
    t_log.each do |valid|
      expect(valid.user_id).to eq(u1.id)
      expect(valid.recommendation).to eq("recommended")
    end
  end

  it "user is not recommended to a training" do
    u1 = FactoryGirl.create(:user)
    t = FactoryGirl.create(:training)

    t_log = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended')
    expect(t_log).to be_empty
  end

  it "user is invited to a training" do
    u1 = FactoryGirl.create(:user)
    t = FactoryGirl.create(:training)
    reco = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended').first_or_create()

    t_log = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended')
    expect(t_log).to_not be_empty
    t_log.update(recommendation: 'invited')
    t_log.each do |valid|
      expect(valid.user_id).to eq(u1.id)
      expect(valid.recommendation).to eq("invited")
    end
  end

  it "user is not invited to a training" do
    u1 = FactoryGirl.create(:user)
    t = FactoryGirl.create(:training)
    reco = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended').first_or_create()

    t_log = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended')
    expect(t_log).to_not be_empty
    t_log.each do |valid|
      expect(valid.user_id).to eq(u1.id)
      expect(valid.recommendation).to_not eq("invited")
    end
  end

  it "user is going to a training (accepts invitation)" do
    u1 = FactoryGirl.create(:user)
    t = FactoryGirl.create(:training)
    reco = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended').first_or_create()

    t_log = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended')
    expect(t_log).to_not be_empty
    t_log.update(recommendation: 'invited')
    t_log.each do |valid|
      expect(valid.user_id).to eq(u1.id)
      expect(valid.recommendation).to eq("invited")
    end
    t_log.update(recommendation: 'going')
    t_log.each do |valid|
      expect(valid.user_id).to eq(u1.id)
      expect(valid.recommendation).to eq("going")
    end
  end

  it "user is not going to a training (declines invitation)" do
    u1 = FactoryGirl.create(:user)
    t = FactoryGirl.create(:training)
    reco = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended').first_or_create()

    t_log = TrainingLog.where(user_id: u1.id, training_id: t.id, recommendation: 'recommended')
    expect(t_log).to_not be_empty
    t_log.update(recommendation: 'invited')
    t_log.each do |valid|
      expect(valid.user_id).to eq(u1.id)
      expect(valid.recommendation).to eq("invited")
    end
    t_log.update(recommendation: 'not going')
    t_log.each do |valid|
      expect(valid.user_id).to eq(u1.id)
      expect(valid.recommendation).to eq("not going")
    end
  end
end
