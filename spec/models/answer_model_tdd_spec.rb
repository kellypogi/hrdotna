require 'rails_helper'

describe "Answer Model" do
  it "saves when comp_level is blank and imp_level is 1" do
    answer = FactoryGirl.create(:answer)
    answer.update(comp_level: " ", imp_level: "1")
    expect(answer.comp_level).to eq(" ")
    expect(answer.imp_level).to eq("1")
  end

  it "saves when imp_level is blank and comp_level is 1" do
    answer = FactoryGirl.create(:answer)
    answer.update(imp_level: " ", comp_level: "1")
    expect(answer.imp_level).to eq(" ")
    expect(answer.comp_level).to eq("1")
  end

  it "saves when imp_level is 1 and comp_level is 1" do
    answer = FactoryGirl.create(:answer)
    answer.update(imp_level: "1", comp_level: "1")
    expect(answer.imp_level).to eq("1")
    expect(answer.comp_level).to eq("1")
  end

  it "saves when imp_level is 2 and comp_level is 3" do
    answer = FactoryGirl.create(:answer)
    answer.update(imp_level: "2", comp_level: "3")
    expect(answer.imp_level).to eq("2")
    expect(answer.comp_level).to eq("3")
  end

  it "saves when imp_level is 4 and comp_level is 4" do
    answer = FactoryGirl.create(:answer)
    answer.update(imp_level: "4", comp_level: "4")
    expect(answer.imp_level).to eq("4")
    expect(answer.comp_level).to eq("4")
  end

end
