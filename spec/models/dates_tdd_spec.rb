require 'rails_helper'

describe "Dates Test" do
  it "form can be viewed by user if deadline > Date.today" do
    form = FactoryGirl.create(:form)
    ques = FactoryGirl.create(:question)
    form_ques = FactoryGirl.create(:forms_question)
    form.update(deadline: Date.today+3.month)
    expect(form.deadline).to eq(Date.today+3.month)
    form_ques.update(form_id: form.id, question_id:ques.id)
    expect(form_ques.form_id).to eq(form.id)
    expect(form_ques.question_id).to eq(ques.id)

    forms = Form.where("deadline > ?", Date.today)
    expect(forms).to_not be_empty
    form_groups = FormsQuestion.select("form_id").group(:form_id).distinct
    expect(form_groups).to_not be_empty

    form_view = forms.where(id: form_groups)
    expect(form_view).to_not be_empty
    present = form_view.where(id: form.id)
    expect(present).to_not be_empty
  end

  it "form can't be viewed by user if deadline < Date.today" do
    form = FactoryGirl.create(:form)
    ques = FactoryGirl.create(:question)
    form_ques = FactoryGirl.create(:forms_question)
    form.update(deadline: Date.today-3.month)
    expect(form.deadline).to eq(Date.today-3.month)
    form_ques.update(form_id: form.id, question_id:ques.id)
    expect(form_ques.form_id).to eq(form.id)
    expect(form_ques.question_id).to eq(ques.id)

    forms = Form.where("deadline > ?", Date.today)
    form_groups = FormsQuestion.select("form_id").group(:form_id).distinct

    form_view = forms.where(id: form_groups)
    present = form_view.where(id: form.id)
    expect(present).to be_empty
  end

  it "[in user profile: trainings attended] finished training can be viewed" do
    user = FactoryGirl.create(:user)
    training = FactoryGirl.create(:training)
    training.update(date: Date.today-3.year)
    expect(training.date).to eq(Date.today-3.year)

    t = FactoryGirl.create(:training_log)
    t.update(user_id: user.id, training_id: training.id, recommendation: "Going")
    expect(t.user_id).to eq(user.id)
    expect(t.training_id).to eq(training.id)
    expect(t.recommendation).to eq("Going")

    date_before = Date.today-50.year..Date.today
    trainingsAttended = TrainingLog.joins(:training).where(user_id: user.id, recommendation: "Going", trainings: {date: date_before})
    expect(trainingsAttended).to_not be_empty
  end

  it "[in user profile: trainings attended] unfinished training cannot be viewed" do
    user = FactoryGirl.create(:user)
    training = FactoryGirl.create(:training)
    training.update(date: Date.today+3.year)
    expect(training.date).to eq(Date.today+3.year)

    t = FactoryGirl.create(:training_log)
    t.update(user_id: user.id, training_id: training.id, recommendation: "Going")
    expect(t.user_id).to eq(user.id)
    expect(t.training_id).to eq(training.id)
    expect(t.recommendation).to eq("Going")

    date_before = Date.today-50.year..Date.today
    trainingsAttended = TrainingLog.joins(:training).where(user_id: user.id, recommendation: "Going", trainings: {date: date_before})
    expect(trainingsAttended).to be_empty
  end

end
