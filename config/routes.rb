Rails.application.routes.draw do

  resources :trainings
  get 'users/show'

  get 'users/index'

  get 'home/show'

  get 'sessions/create'

  get 'sessions/destroy'

  get 'welcome/index'

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
  get 'forms_questions/submit', to: 'forms_questions#submit'

  resources :sessions, only: [:create, :destroy]
  resource :home, only: [:show]
  resources :answers
  resources :forms_questions
  resources :forms
  resources :questions
  resources :recommendations
  resources :recommended_list
  # resources :users, only: [:show, :index]
  get 'profile', to: 'profiles#index'
  get 'profile/edit', to: 'profiles#edit'
  patch 'profile/update', to: 'profiles#update'
  post 'tranings/invite/', to: 'trainings#invite'
  post 'tranings/reject/', to: 'trainings#reject'
  post 'tranings/accept/', to: 'trainings#accept'
  post 'tranings/deny/', to: 'trainings#deny'
  post 'users/set_admin/', to: 'users#set_admin'
  post 'users/unset_admin/', to: 'users#unset_admin'
  get 'users/:user_id' => 'users#show', as: :usersshow
  root to: "home#show"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
