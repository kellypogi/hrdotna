class CreateFormsQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :forms_questions do |t|
      t.belongs_to :form, index: true
      t.belongs_to :question, index: true
      t.string :area
      t.string :skill

      t.timestamps
    end
  end
end
