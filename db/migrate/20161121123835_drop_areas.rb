class DropAreas < ActiveRecord::Migration[5.0]
  def change
    drop_table(:areas, if_exists: true)
  end
end
