class DropSkills < ActiveRecord::Migration[5.0]
  def change
    drop_table(:skills, if_exists: true)
  end
end
