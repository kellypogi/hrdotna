class SetDefaultStatus < ActiveRecord::Migration[5.0]
  def change
    change_column :forms, :status, :string, default: "Ongoing"
  end
end
