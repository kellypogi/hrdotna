class FixQuestions < ActiveRecord::Migration[5.0]
  def change
    drop_table  :questions, if_exists: true
    create_table :questions do |t|
      t.text :questionDescription
      t.string :area
      t.string :skill

      t.timestamps
    end
  end
end
