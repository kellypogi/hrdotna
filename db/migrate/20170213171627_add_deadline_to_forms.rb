class AddDeadlineToForms < ActiveRecord::Migration[5.0]
  def change
    add_column :forms, :deadline, :timestamp
  end
end
