class SetDefaultToStatus < ActiveRecord::Migration[5.0]
  def change
    def self.up
      change_column :status, :default => "ongoing"
    end
  end
end
