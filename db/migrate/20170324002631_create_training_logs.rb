class CreateTrainingLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :training_logs do |t|
      t.string :recommendation

      t.timestamps
    end
  end
end
