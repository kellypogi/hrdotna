class ChangeDataTypeForEmployeeNumber < ActiveRecord::Migration[5.0]
  def up
    change_column :users, :employee_number, :string, :limit => nil
  end

  def down
    change_column :users, :employee_number, :integer
  end
end
