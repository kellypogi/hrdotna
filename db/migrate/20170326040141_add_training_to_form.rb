class AddTrainingToForm < ActiveRecord::Migration[5.0]
  def change
    add_reference :forms, :training, foreign_key: true
  end
end
