class AddDefaultsToUser < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :full_name, :string , :default => "Dela Cruz, Juan"
  end
end
