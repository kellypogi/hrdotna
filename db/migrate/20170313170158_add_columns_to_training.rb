class AddColumnsToTraining < ActiveRecord::Migration[5.0]
  def change
    add_column :trainings, :title, :string
    add_column :trainings, :assoc_skill, :string
    add_column :trainings, :date, :datetime
    add_column :trainings, :description, :text
    add_column :trainings, :venue, :string
    add_column :trainings, :attendees, :string
    add_column :trainings, :status, :string
  end
end
