class AddDetailsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :year, :string
    add_column :users, :month, :string
  end
end
