class AddDefaultToTrainings < ActiveRecord::Migration[5.0]
  def change
    change_column :trainings, :status, :string, :default => "Open"
    change_column :trainings, :description, :text, :default => "No description available."
    change_column :trainings, :assoc_skill, :string, :default => "No skill yet."
  end
end
