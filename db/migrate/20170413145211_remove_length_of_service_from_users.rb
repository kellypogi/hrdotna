class RemoveLengthOfServiceFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :length_of_service, :string
  end
end
