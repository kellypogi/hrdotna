class DropTnaForms < ActiveRecord::Migration[5.0]
  def change
    drop_table(:tna_forms, if_exists: true)
  end
end
