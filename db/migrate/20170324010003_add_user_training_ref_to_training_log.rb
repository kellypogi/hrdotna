class AddUserTrainingRefToTrainingLog < ActiveRecord::Migration[5.0]
  def change
    add_reference :training_logs, :user, foreign_key: true
    add_reference :training_logs, :training, foreign_key: true
  end
end
