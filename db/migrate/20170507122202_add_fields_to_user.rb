class AddFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :key_function, :string
    add_column :users, :classification, :string
  end
end
