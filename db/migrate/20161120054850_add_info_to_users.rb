class AddInfoToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :date_stamp, :date
    add_column :users, :tna_stamp, :string
    add_column :users, :employee_number, :bigint
    add_column :users, :full_name, :string
    add_column :users, :birth_date, :date
    add_column :users, :sex, :string
    add_column :users, :marital_status, :string
    add_column :users, :unit, :string
    add_column :users, :designation, :string
    add_column :users, :length_of_service, :string
  end
end
