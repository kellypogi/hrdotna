class AddQuestionIdToTrainings < ActiveRecord::Migration[5.0]
  def change
    add_reference :trainings, :question, index: true
  end
end
