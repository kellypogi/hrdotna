class CreateAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :answers do |t|
      t.string :comp_level
      t.string :imp_level
      t.belongs_to :user
      t.string :user_id
      t.belongs_to :forms_question, index: true

      t.timestamps
    end
  end
end
