class AddFormRefToTraining < ActiveRecord::Migration[5.0]
  def change
    add_reference :trainings, :form, foreign_key: true
  end
end
