class AddDescriptionToForms < ActiveRecord::Migration[5.0]
  def change
    add_column :forms, :description, :text
  end
end
