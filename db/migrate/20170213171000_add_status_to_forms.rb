class AddStatusToForms < ActiveRecord::Migration[5.0]
  def change
    add_column :forms, :status, :string
  end
end
