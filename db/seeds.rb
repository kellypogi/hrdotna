# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Form.create(title: 'Temp Form 1')
Question.create(questionDescription: 'Question 1', area: 'Sample Area 1', skill: 'Sample Skill 1')
require 'csv'

csv_text= File.read(Rails.root.join('lib', 'seeds', 'Profiles.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  u = User.new
  u.date_stamp = row['Date Stamp']
  u.tna_stamp = row['TNA Stamp']
  u.employee_number = row['Employee Number']
  u.full_name = row['Name'] rescue nil
  date = row['Date of Birth']
  if date != nil
    if date.include? "/"
      u.birth_date = Date.strptime(date,"%m/%d/%Y") rescue nil
    elsif date.include? "-"
      u.birth_date = Date.strptime(date,"%m-%d-%Y") rescue nil
    else
      u.birth_date = nil
    end
  end
  if row['Sex'] == 'M'
    u.sex = 'Male'
  elsif row['Sex'] == 'F'
    u.sex = 'Female'
  end
  u.marital_status = row['Marital Status'] rescue nil
  u.unit = row['Unit']
  u.designation =row['Designation']
  u.year = row['Length of Service']
  u.save
  u.errors.full_messages.each do|message|
    puts message
  end
  puts "#{u.full_name}, #{u.marital_status} saved"
end

csv_text= File.read(Rails.root.join('lib', 'seeds', 'Profiles1.csv'))
csv= CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  u = User.new
  u.employee_number = row['Employee Number']
  u.email = row['UP Mail']
  u.full_name = row['Full Name']
  u.save
  puts "#{u.full_name} saved"
end

csv_text= File.read(Rails.root.join('lib', 'seeds', 'Profiles2.csv'))
csv= CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  u = User.new
  u.employee_number = row['Employee Number']
  u.email = row['UP Mail']
  u.full_name = row['Full Name']
  u.save
  puts "#{u.full_name} saved"
end



puts "There are now #{User.count} rows in your users table"
