# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Form.create(title: 'Temp Form 1')
Question.create(questionDescription: 'Question 1', area: 'Sample Area 1', skill: 'Sample Skill 1')
require 'csv'

csv_text= File.read(Rails.root.join('lib', 'seeds', 'Test.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  u = User.new
  u.date_stamp = row['Date Stamp']
  u.tna_stamp = row['TNA Stamp']
  u.employee_number = row['Employee Number']
  u.full_name = row['Name']
  u.birth_date = row['Date of Birth']
  u.sex = row['Sex']
  u.marital_status = row['Marital Status']
  u.unit = row['Unit']
  u.designation =row['Designation']
  u.year = row['Length of Service']
  u.save
  puts "#{u.full_name}, #{u.marital_status} saved"
end
