# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170508114103) do

  create_table "answers", force: :cascade do |t|
    t.string   "comp_level"
    t.string   "imp_level"
    t.string   "user_id"
    t.integer  "forms_question_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["forms_question_id"], name: "index_answers_on_forms_question_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
  end

  create_table "forms", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "status",      default: "Ongoing"
    t.datetime "deadline"
    t.text     "description"
    t.integer  "training_id"
    t.index ["training_id"], name: "index_forms_on_training_id"
  end

  create_table "forms_questions", force: :cascade do |t|
    t.integer  "form_id"
    t.integer  "question_id"
    t.string   "area"
    t.string   "skill"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["form_id"], name: "index_forms_questions_on_form_id"
    t.index ["question_id"], name: "index_forms_questions_on_question_id"
  end

  create_table "questions", force: :cascade do |t|
    t.text     "questionDescription"
    t.string   "area"
    t.string   "skill"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "training_logs", force: :cascade do |t|
    t.string   "recommendation"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
    t.integer  "training_id"
    t.index ["training_id"], name: "index_training_logs_on_training_id"
    t.index ["user_id"], name: "index_training_logs_on_user_id"
  end

  create_table "trainings", force: :cascade do |t|
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "title"
    t.string   "assoc_skill", default: "No skill yet."
    t.datetime "date"
    t.text     "description", default: "No description available."
    t.string   "venue"
    t.string   "attendees"
    t.string   "status",      default: "Open"
    t.integer  "form_id"
    t.integer  "question_id"
    t.index ["form_id"], name: "index_trainings_on_form_id"
    t.index ["question_id"], name: "index_trainings_on_question_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.date     "date_stamp"
    t.string   "tna_stamp"
    t.string   "employee_number"
    t.string   "full_name",        default: "Dela Cruz, Juan"
    t.date     "birth_date"
    t.string   "sex"
    t.string   "marital_status"
    t.string   "unit"
    t.string   "designation"
    t.boolean  "is_admin",         default: false
    t.string   "email"
    t.string   "year"
    t.string   "month"
    t.string   "key_function"
    t.string   "classification"
    t.text     "answered"
  end

end
