# -*- encoding: utf-8 -*-
# stub: jquery-datatables 1.10.13 ruby lib

Gem::Specification.new do |s|
  s.name = "jquery-datatables"
  s.version = "1.10.13"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["mkhairi"]
  s.date = "2016-12-15"
  s.description = "Include jQuery DataTables in asset pipeline with ease"
  s.email = ["mkhairi@labs.my"]
  s.homepage = "https://github.com/mkhairi/jquery-datatables"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.5.1"
  s.summary = "Jquery DataTables web assets for Rails, etc."

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<actionpack>, ["~> 4.1"])
      s.add_development_dependency(%q<activesupport>, ["~> 4.1"])
      s.add_development_dependency(%q<sprockets-rails>, ["~> 2.3"])
      s.add_development_dependency(%q<jquery-rails>, ["~> 3.1"])
      s.add_development_dependency(%q<bundler>, ["~> 1.7"])
      s.add_development_dependency(%q<rake>, ["~> 10.0"])
    else
      s.add_dependency(%q<actionpack>, ["~> 4.1"])
      s.add_dependency(%q<activesupport>, ["~> 4.1"])
      s.add_dependency(%q<sprockets-rails>, ["~> 2.3"])
      s.add_dependency(%q<jquery-rails>, ["~> 3.1"])
      s.add_dependency(%q<bundler>, ["~> 1.7"])
      s.add_dependency(%q<rake>, ["~> 10.0"])
    end
  else
    s.add_dependency(%q<actionpack>, ["~> 4.1"])
    s.add_dependency(%q<activesupport>, ["~> 4.1"])
    s.add_dependency(%q<sprockets-rails>, ["~> 2.3"])
    s.add_dependency(%q<jquery-rails>, ["~> 3.1"])
    s.add_dependency(%q<bundler>, ["~> 1.7"])
    s.add_dependency(%q<rake>, ["~> 10.0"])
  end
end
