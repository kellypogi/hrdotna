# HRDO - Training Needs Assessment

    TNA App for UP HRDO

## Installation

Get `rbenv` and use version 2.3.0.

```console
rbenv install 2.3.0
gem update --system
bundle install
```

Clone the repository and install the required gems
```linux
bundle install
```

Load db using `rails db:schema:load` and seed using `rails db:seed`

Run using `rails s`
